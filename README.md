# Vision-AI-TaskPhase
All the work and projects done in the TaskPhase will be uploaded in this repository.

## Task-1
1) Crash Course on Python (Coursera)
2) Game-1 : Snake Game.
3) Tic-Tac-Toe.

## Task-2
1) Numpy
2) Pandas
3) Matplotlib
4) Sklearn
5) Open CV

## Task-3
* Course-1 : Deep Learning & Neural Networks
<-- csw
* course-2 : Improving Deep NN, hyperparametr tuning, optimization
* Course-3 : Structuring ML Projects
* Course-4 : Convolutional Neural Networks
* Course-5 : Sequence Models
