## VISION TASKPHASE Task-1
# 1.	Crash course on Python:
This course clearly explains and includes the following:
-	Basic Python Syntax and semantics
-	Expressions and Variables
-	Different Data Types – like int, float etc.,
-	Various in-built functions and the use of def to create our own function.
-	Conditionals – if-else statements and elif ladder
-	Loops – for loop and while loop
-	Recursion
-	Data structures – lists, tuples and dictionaries
-	Strings
-	Classes and objects
> 
The above concepts can be well understood but might be confusing while using lists and dictionaries with classes and objects. And the final Project in the course can be competed with just a simple code using only conditionals and loops.
> 
Here is the link to my certificate for completing the “Crash Course on Python”:
> https://www.coursera.org/account/accomplishments/certificate/93BG9ZUHYXF4
# 2.	Game-1: Snake Game.
I’ve used ‘Pygame’, ‘Random’ and ‘Math’ libraries of python to write and develop the code for the snake game. I have referred to the Pygame documentation(online), two You Tube videos and had also finished a Coursera Project on writing the python code for a simple snake game.
>
 Here are the links for the YouTube videos I referred:
>
> https://youtu.be/QFvqStqPCRU
>
> https://youtu.be/QFvqStqPCRU
>
 And here is the link to my certificate of the Coursera Project on Snake Game in Python:
>
> https://coursera.org/share/366ebe4533a141f68bb11d9af03ac36b
# 3.	Game-2: Tic-Tac-Toe.
I again used the same “Pygame”, ‘random’, ‘math’ and also “NumPy” library of python to develop the code for this game. I referred to the Pygame documentation and finished the course “Create a tic-tac-toe game in python” in coursera. It is well explained and I clearly understood how to write the code for the game.
>
Here is the link to my coursera certificate:
> 
> https://coursera.org/share/78793d1e32287703fec08ed25f02f559
>
Finally, I’ve got a basic and proper understanding on how to use python and its syntax to write any code.
